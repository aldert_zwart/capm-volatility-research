(**
This is a dedicated script for plotting the returns for the different portfolios.

The "myExcessReturnPorfolios.csv" file is used for the portfolio data. This data is generated in the scriptPortfolio.fsx file

The returns for the value weigthed stock market is retrieved from Ken French data via the Common script.
*)

#r "nuget: FSharp.Stats, 0.4.1"
#r "nuget: FSharp.Data"
#r "nuget: Plotly.NET, 2.0.0-beta9"
#r "nuget:NodaTime"

#load "../data-cache/common.fsx"

open Common
open Plotly.NET
open System
open FSharp.Data
open FSharp.Stats
open NodaTime

Environment.CurrentDirectory <- __SOURCE_DIRECTORY__

let myFactorPorts = CsvProvider<"../Secrets/myExcessReturnPortfolios.csv",
                                ResolutionFolder = __SOURCE_DIRECTORY__>.GetSample()

myFactorPorts.Headers


(**
Let's start with our long-short portfolio.
*)
let long = myFactorPorts.Rows |> Seq.filter(fun row -> row.PortfolioName = "Idiosyncratic Volatility CAPM" && row.Index = Some 1)
let short = myFactorPorts.Rows |> Seq.filter(fun row -> row.PortfolioName = "Idiosyncratic Volatility CAPM" && row.Index = Some 3)

type Return = { YearMonth : DateTime; Return : float; portfolioId : string; }

let longShort =
    // this is joining long to short by YearMonth:DateTime
    let shortMap = short |> Seq.map(fun row -> row.YearMonth, row) |> Map
    long
    |> Seq.map(fun longObs -> 
        match Map.tryFind longObs.YearMonth shortMap with
        | None -> failwith "probably your date variables are not aligned"
        | Some shortObs -> { YearMonth = longObs.YearMonth; Return = longObs.Ret - shortObs.Ret; portfolioId = "LongShort" })
    |> Seq.toArray         


(**
Create a long only portfolio
*)

let longOnly =
    long
    |> Seq.map(fun longObs -> { YearMonth = longObs.YearMonth; Return = longObs.Ret; portfolioId = "longOnly" })
    |> Seq.toArray

(**
Retrieve data from Ken French
*)
let ff3 = French.getFF3 Frequency.Monthly

let monthlyRiskFreeRate =
    ff3
    |> Array.map(fun x -> YearMonth(x.Date.Year,x.Date.Month),x.Rf)
    |> Map.ofArray


let vwMktRf =
    let portfolioMonths = longOnly |> Array.map(fun x -> x.YearMonth)
    let minYm = portfolioMonths |> Array.min
    let maxYm = portfolioMonths |> Array.max
    ff3
    |> Array.map(fun x -> { 
        YearMonth = DateTime(x.Date.Year, x.Date.Month ,x.Date.Day)
        Return = x.MktRf; 
        portfolioId = "MktRf" })
    |> Array.filter(fun x -> 
        x.YearMonth >= minYm &&
        x.YearMonth <= maxYm)


(**
Create fuction to perfom the nomalization of the volatility
*)
let normalizeToTenPct (xs:Return array) =
    let annualizedVol = sqrt(12.0) * (xs |> Seq.stDevBy(fun x -> x.Return))
    xs 
    |> Array.map(fun x -> 
        { x with Return = (0.1/annualizedVol) * x.Return })

(**
Create a function to re-write the returns in cumulative returns 
*)
let cumulateReturn (xs: Return array) =
    let mapper (priorRet:float) (thisObservation:Return) =
        let asOfNow = priorRet*(1.0 + thisObservation.Return)
        { thisObservation with Return = asOfNow}, asOfNow
    // remember to make sure that your sort order is correct.
    let sorted = xs |> Array.sortBy(fun x -> x.YearMonth)
    (1.0, sorted) 
    ||> Array.mapFold mapper 
    |> fst   


(**
Test a plot for the long only portfolio
*)
let longOnlyPlot =
    longOnly
    |> cumulateReturn
    |> Array.map(fun x -> DateTime(x.YearMonth.Year,x.YearMonth.Month,1), x.Return)
    |> Chart.Line 
    |> Chart.withTitle "Long only plot"
    |> Chart.Show


(**
Create a plotting function
*)
let portfolioReturnPlot (xs:Return array) =
    xs
    |> Array.map(fun x -> DateTime(x.YearMonth.Year,x.YearMonth.Month,1), x.Return)
    |> Chart.Line 
    |> Chart.withTitle "Growth of 1 Euro"


(**
Create a chart for the cumulativer returns
*)
let combinedChartCum =
    Array.concat [longShort; longOnly; vwMktRf]
    |> Array.groupBy(fun x -> x.portfolioId)
    |> Array.map(fun (portfolioId, xs) ->
        xs
        |> cumulateReturn
        |> portfolioReturnPlot
        |> Chart.withTraceName (portfolioId.ToString()))
    |> Chart.Combine
    |> Chart.withTitle "Cumulative returns"
    |> Chart.Show


(**
Create a chart for the fixe leverage portfolio
*)
let combinedChartTenLev =
    Array.concat [longShort; longOnly; vwMktRf]
    |> Array.groupBy(fun x -> x.portfolioId)
    |> Array.map(fun (portfolioId, xs) ->
        xs
        |> normalizeToTenPct
        |> cumulateReturn
        |> portfolioReturnPlot
        |> Chart.withTraceName (portfolioId.ToString()))
    |> Chart.Combine
    |> Chart.withTitle "Cumulative returns with constant leverage"
    |> Chart.Show
    

