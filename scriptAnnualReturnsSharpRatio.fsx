(**
This code is generated in order to calculate the annual Average Returnsand Sharp Ratios
*)

#r "nuget: FSharp.Stats, 0.4.1"
#r "nuget: FSharp.Data"
#r "nuget: Plotly.NET, 2.0.0-beta9"

#load "../data-cache/common.fsx"
#load "../functions/Portfolio.fsx"

open Plotly.NET
open System
open FSharp.Data
open Common
open Portfolio
open FSharp.Stats

Environment.CurrentDirectory <- __SOURCE_DIRECTORY__
(**
We get the Fama-French 3-Factor asset pricing model data.
*)
let ff3 = French.getFF3 Frequency.Monthly
(**
Let's get our factor data.
*)
let myFactorPorts = CsvProvider<"../Secrets/myExcessReturnPortfolios.csv",
                                ResolutionFolder = __SOURCE_DIRECTORY__>.GetSample()
(**
[ML.NET](https://docs.microsoft.com/en-us/dotnet/machine-learning/) is a .NET (C#/F#/VB.NET) machine learning library. There are several [tutorials](https://dotnet.microsoft.com/learn/ml-dotnet) and many F# examples in the sample github repository [here](https://github.com/dotnet/machinelearning-samples/tree/main/samples/fsharp/getting-started).

We will use ML.NET for Ordinary Least Squares (OLS) regression, but you can also do pretty fancy machine learning models with it. So think of it as a gentle introduction giving you some guidance on how to use ML.NET. This will help if you want to experiment with fancy machine learning models after you're done with this course.
*)
#r "nuget:Microsoft.ML,1.5"
#r "nuget:Microsoft.ML.MKL.Components,1.5"

myFactorPorts.Headers

open Microsoft.ML
open Microsoft.ML.Data
(**
Let's start with our long-short portfolio.
*)
let long = myFactorPorts.Rows |> Seq.filter(fun row -> row.PortfolioName = "Idiosyncratic Volatility CAPM" && row.Index = Some 1)
let short = myFactorPorts.Rows |> Seq.filter(fun row -> row.PortfolioName = "Idiosyncratic Volatility CAPM" && row.Index = Some 3)

(**
Create the data objective for the different portfolios and time periods
*)
let longOnly = 
    myFactorPorts.Rows 
    |> Seq.filter(fun row -> row.PortfolioName = "Idiosyncratic Volatility CAPM" && row.Index = Some 1)
    |> Seq.map(fun x -> { Symbol = "LongOnly"; Date = x.YearMonth; Return = x.Ret })
    |> Seq.toArray


let longOnlyFirst = 
    myFactorPorts.Rows 
    |> Seq.filter(fun row -> row.YearMonth < DateTime(2011,1,1))
    |> Seq.filter(fun row -> row.PortfolioName = "Idiosyncratic Volatility CAPM" && row.Index = Some 1)
    |> Seq.map(fun x -> { Symbol = "LongOnly"; Date = x.YearMonth; Return = x.Ret })
    |> Seq.toArray

let longOnlySecond =
    myFactorPorts.Rows 
    |> Seq.filter(fun row -> row.YearMonth >= DateTime(2011,1,1))
    |> Seq.filter(fun row -> row.PortfolioName = "Idiosyncratic Volatility CAPM" && row.Index = Some 1)
    |> Seq.map(fun x -> { Symbol = "LongOnly"; Date = x.YearMonth; Return = x.Ret })
    |> Seq.toArray 

type Return = { YearMonth : DateTime; Return : float }
let longShort =
    // this is joining long to short by YearMonth:DateTime
    let shortMap = short |> Seq.map(fun row -> row.YearMonth, row) |> Map
    long
    |> Seq.map(fun longObs -> 
        match Map.tryFind longObs.YearMonth shortMap with
        | None -> failwith "probably your date variables are not aligned"
        | Some shortObs -> { YearMonth = longObs.YearMonth; Return = longObs.Ret - shortObs.Ret })
    |> Seq.toArray    
(**
Sharp ratio Long Only
*)
(**
The data that is feeded is Monthly data
*)

(**
Calculate the Sharp Ratio and annual returns, the returns are already adjusted to the riskfree rate
*)
let lenLongShort =
   longShort
   |> Seq.length

let lenLongOnly =
   longOnly
   |> Seq.length

let longShortFirst =
   longShort
   |> Seq.truncate(lenLongShort/2)

let longShortSecond =
   longShort
   |> Seq.skip(lenLongShort/2)
   
(**
Calculate Annual Return Long Short
*)
let AnnualReturnLongShort = 
   longShort
   |> Seq.map(fun x -> x.Return)
   |> Seq.mean
   |> fun monthlyMean -> 12.0 * monthlyMean

let AnnualReturnLongShortFirst = 
   longShortFirst
   |> Seq.map(fun x -> x.Return)
   |> Seq.mean
   |> fun monthlyMean -> 12.0 * monthlyMean

let AnnualReturnLongShortSecond = 
   longShortSecond
   |> Seq.map(fun x -> x.Return)
   |> Seq.mean
   |> fun monthlyMean -> 12.0 * monthlyMean

(**
Calculate Annual Return Long Only
*)
let AnnualReturnLongOnly = 
   longOnly
   |> Seq.map(fun x -> x.Return)
   |> Seq.mean
   |> fun monthlyMean -> 12.0 * monthlyMean

let AnnualReturnLongOnlyFirst = 
   longOnlyFirst
   |> Seq.map(fun x -> x.Return)
   |> Seq.mean
   |> fun monthlyMean -> 12.0 * monthlyMean

let AnnualReturnLongOnlySecond = 
   longOnlySecond
   |> Seq.map(fun x -> x.Return)
   |> Seq.mean
   |> fun monthlyMean -> 12.0 * monthlyMean

(**
Sharp ratio Long Only
*)
let AnnualSharpRatioLongOnly = 
   longOnly
   |> Seq.map(fun x -> x.Return)
   |> Seq.stDev
   |> fun monthlyMean -> sqrt(12.0) * monthlyMean

let AnnualSharpRatioLongOnlyFirst = 
   longOnlyFirst
   |> Seq.map(fun x -> x.Return)
   |> Seq.stDev
   |> fun monthlyMean -> sqrt(12.0) * monthlyMean

let AnnualSharpRatioLongOnlySecond = 
   longOnlySecond
   |> Seq.map(fun x -> x.Return)
   |> Seq.stDev
   |> fun monthlyMean -> sqrt(12.0) * monthlyMean

(**
Sharp ratio Long Short
*)
let AnnualSharpRatioLongShort = 
   longShort
   |> Seq.map(fun x -> x.Return)
   |> Seq.stDev
   |> fun monthlyMean -> sqrt(12.0) * monthlyMean

let AnnualSharpRatioLongShortFirst = 
   longShortFirst
   |> Seq.map(fun x -> x.Return)
   |> Seq.stDev
   |> fun monthlyMean -> sqrt(12.0) * monthlyMean

let AnnualSharpRatioLongShortSecond = 
   longShortSecond
   |> Seq.map(fun x -> x.Return)
   |> Seq.stDev
   |> fun monthlyMean -> sqrt(12.0) * monthlyMean
